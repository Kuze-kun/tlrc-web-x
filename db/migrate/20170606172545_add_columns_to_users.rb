class AddColumnsToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :first_name, :string
  	add_column :users, :middle_name, :string
  	add_column :users, :last_name, :string
  	add_column :users, :accountability, :decimal, precision: 5, scale: 2, :default => 0
  end
end
