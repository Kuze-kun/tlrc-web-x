# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170607030653) do

  create_table "announcements", force: :cascade do |t|
    t.string   "announcement"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "documents", force: :cascade do |t|
    t.integer  "users_id"
    t.string   "file_name"
    t.string   "attachment"
    t.string   "size",                                       default: "short"
    t.integer  "number_of_pages"
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.text     "note"
    t.string   "document_type",                              default: "word"
    t.string   "print_status",                               default: "waiting"
    t.string   "transaction_status",                         default: "unpaid"
    t.decimal  "price",              precision: 5, scale: 2, default: 0.0
    t.integer  "number_of_copies",                           default: 1
    t.string   "page_range",                                 default: "all"
    t.string   "orientation",                                default: "portrait"
    t.string   "color",                                      default: "colored"
    t.string   "slide_layout",                               default: "full-page"
    t.integer  "pages_per_paper",                            default: 1
  end

  add_index "documents", ["users_id"], name: "index_documents_on_users_id"

  create_table "users", force: :cascade do |t|
    t.string  "user_id"
    t.string  "primary_key"
    t.string  "password"
    t.string  "first_name"
    t.string  "middle_name"
    t.string  "last_name"
    t.decimal "accountability", precision: 5, scale: 2, default: 0.0
  end

end
