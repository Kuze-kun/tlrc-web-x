module SessionsHelper
	def log_in(user)
		session[:user_id] = user.user_id
	end

	def current_user
		@current_user ||= User.find_by(user_id: session[:user_id])
	end

	def logged_in?
		!current_user.nil?
	end

	def log_out
		session.delete(session[:user_id])
		reset_session
		@current_user = nil
	end

	def is_admin?
		if logged_in?
			return @current_user.user_id == 'admin'
		else
			return false
		end
	end

	def is_owner? document_id
		if logged_in?
			return @current_user.id == document_id
		else
			return false
		end
	end
end